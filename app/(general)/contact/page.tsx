export const metadata = {
  title: "Contact",
  description: "Contact",
  keywords: ["Contact", "Next.js", "React.js", "Typescript"],
  robots: {
    index: true,
    follow: true,
    googleBot: {
      index: true,
      follow: true,
    },
  },
};

export default function Contact() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <span className="text-5xl">Contact</span>
    </main>
  );
}
