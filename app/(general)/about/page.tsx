import type { Metadata } from "next";

export const metadata: Metadata = {
  title: "About",
  description: "About",
  keywords: ["About", "Next.js", "React.js", "Typescript"],
};

export default function About() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <span className="text-5xl">About</span>
    </main>
  );
}
