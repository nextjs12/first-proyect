import { ActiveLink } from "../activeLink/ActiveLink";

const navItems = [
  {
    path: "/",
    name: "Home",
  },
  {
    path: "/about",
    name: "About",
  },
  {
    path: "/pricing",
    name: "Pricing",
  },
  {
    path: "/contact",
    name: "Contact",
  },
];

export const Navbar = () => {
  return (
    <nav className="flex justify-between bg-blue-300 bg-opacity-10 p-2 m-2 rounded">
      <h1>Next</h1>
      <ul className="flex gap-5">
        {navItems.map((item) => (
          <ActiveLink key={item.path} path={item.path} text={item.name} />
        ))}
      </ul>
    </nav>
  );
};
