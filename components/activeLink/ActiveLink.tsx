"use client";

import Link from "next/link";
import style from "./ActiveLink.module.css";
import { usePathname } from "next/navigation";

interface Props {
  path: string;
  text: string;
}

export const ActiveLink = ({ path, text }: Props) => {
  const isActive = usePathname();

  return (
    <Link
      className={`${style.link} ${isActive === path && style["link-active"]}`}
      href={path}
    >
      {text}
    </Link>
  );
};
